package com.formation.FoodTruckAutoGen.Entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the actualités database table.
 * 
 */
@Entity
@Table(name="actualités")
@NamedQuery(name="Actualité.findAll", query="SELECT a FROM Actualité a")
public class Actualité implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_actualite")
	private int idActualite;

	@Lob
	private String description;

	private String image;

	private String titre;

	public Actualité() {
	}

	public int getIdActualite() {
		return this.idActualite;
	}

	public void setIdActualite(int idActualite) {
		this.idActualite = idActualite;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getTitre() {
		return this.titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

}