package com.formation.FoodTruckAutoGen.Entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the adresse database table.
 * 
 */
@Entity
@NamedQuery(name="Adresse.findAll", query="SELECT a FROM Adresse a")
public class Adresse implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_adresse")
	private int idAdresse;

	private byte activite;

	@Column(name="code_postal")
	private int codePostal;

	@Column(name="id_adresse_type")
	private int idAdresseType;

	@Column(name="id_utilisateur")
	private int idUtilisateur;

	@Column(name="numero_de_rue")
	private byte numeroDeRue;

	private String pays;

	private String rue;

	private String ville;

	public Adresse() {
	}

	public int getIdAdresse() {
		return this.idAdresse;
	}

	public void setIdAdresse(int idAdresse) {
		this.idAdresse = idAdresse;
	}

	public byte getActivite() {
		return this.activite;
	}

	public void setActivite(byte activite) {
		this.activite = activite;
	}

	public int getCodePostal() {
		return this.codePostal;
	}

	public void setCodePostal(int codePostal) {
		this.codePostal = codePostal;
	}

	public int getIdAdresseType() {
		return this.idAdresseType;
	}

	public void setIdAdresseType(int idAdresseType) {
		this.idAdresseType = idAdresseType;
	}

	public int getIdUtilisateur() {
		return this.idUtilisateur;
	}

	public void setIdUtilisateur(int idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}

	public byte getNumeroDeRue() {
		return this.numeroDeRue;
	}

	public void setNumeroDeRue(byte numeroDeRue) {
		this.numeroDeRue = numeroDeRue;
	}

	public String getPays() {
		return this.pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	public String getRue() {
		return this.rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public String getVille() {
		return this.ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

}