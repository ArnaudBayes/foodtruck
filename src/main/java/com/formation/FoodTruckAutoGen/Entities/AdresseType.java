package com.formation.FoodTruckAutoGen.Entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the adresse_type database table.
 * 
 */
@Entity
@Table(name="adresse_type")
@NamedQuery(name="AdresseType.findAll", query="SELECT a FROM AdresseType a")
public class AdresseType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_adresse_type")
	private int idAdresseType;

	private String libelle;

	public AdresseType() {
	}

	public int getIdAdresseType() {
		return this.idAdresseType;
	}

	public void setIdAdresseType(int idAdresseType) {
		this.idAdresseType = idAdresseType;
	}

	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

}