package com.formation.FoodTruckAutoGen.Entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the commandes database table.
 * 
 */
@Entity
@Table(name="commandes")
@NamedQuery(name="Commande.findAll", query="SELECT c FROM Commande c")
public class Commande implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_commande")
	private int idCommande;

	@Temporal(TemporalType.DATE)
	@Column(name="date_commande")
	private Date dateCommande;

	@Column(name="id_adresse_type")
	private int idAdresseType;

	@Column(name="id_statut_commande")
	private int idStatutCommande;

	@Column(name="id_utilisateur")
	private int idUtilisateur;

	@Column(name="numero_facture")
	private String numeroFacture;

	@Column(name="prix_total")
	private short prixTotal;

	public Commande() {
	}

	public int getIdCommande() {
		return this.idCommande;
	}

	public void setIdCommande(int idCommande) {
		this.idCommande = idCommande;
	}

	public Date getDateCommande() {
		return this.dateCommande;
	}

	public void setDateCommande(Date dateCommande) {
		this.dateCommande = dateCommande;
	}

	public int getIdAdresseType() {
		return this.idAdresseType;
	}

	public void setIdAdresseType(int idAdresseType) {
		this.idAdresseType = idAdresseType;
	}

	public int getIdStatutCommande() {
		return this.idStatutCommande;
	}

	public void setIdStatutCommande(int idStatutCommande) {
		this.idStatutCommande = idStatutCommande;
	}

	public int getIdUtilisateur() {
		return this.idUtilisateur;
	}

	public void setIdUtilisateur(int idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}

	public String getNumeroFacture() {
		return this.numeroFacture;
	}

	public void setNumeroFacture(String numeroFacture) {
		this.numeroFacture = numeroFacture;
	}

	public short getPrixTotal() {
		return this.prixTotal;
	}

	public void setPrixTotal(short prixTotal) {
		this.prixTotal = prixTotal;
	}

}