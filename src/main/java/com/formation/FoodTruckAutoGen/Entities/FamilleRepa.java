package com.formation.FoodTruckAutoGen.Entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the famille_repas database table.
 * 
 */
@Entity
@Table(name="famille_repas")
@NamedQuery(name="FamilleRepa.findAll", query="SELECT f FROM FamilleRepa f")
public class FamilleRepa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_famille_repas")
	private int idFamilleRepas;

	private byte activite;

	private String libelle;

	public FamilleRepa() {
	}

	public int getIdFamilleRepas() {
		return this.idFamilleRepas;
	}

	public void setIdFamilleRepas(int idFamilleRepas) {
		this.idFamilleRepas = idFamilleRepas;
	}

	public byte getActivite() {
		return this.activite;
	}

	public void setActivite(byte activite) {
		this.activite = activite;
	}

	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

}