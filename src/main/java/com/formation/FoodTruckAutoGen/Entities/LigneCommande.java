package com.formation.FoodTruckAutoGen.Entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ligne_commande database table.
 * 
 */
@Entity
@Table(name="ligne_commande")
@NamedQuery(name="LigneCommande.findAll", query="SELECT l FROM LigneCommande l")
public class LigneCommande implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_ligne_commande")
	private int idLigneCommande;

	@Column(name="id_commande")
	private int idCommande;

	@Column(name="id_produit")
	private int idProduit;

	@Column(name="prix_unitaire")
	private short prixUnitaire;

	private short quantite;

	public LigneCommande() {
	}

	public int getIdLigneCommande() {
		return this.idLigneCommande;
	}

	public void setIdLigneCommande(int idLigneCommande) {
		this.idLigneCommande = idLigneCommande;
	}

	public int getIdCommande() {
		return this.idCommande;
	}

	public void setIdCommande(int idCommande) {
		this.idCommande = idCommande;
	}

	public int getIdProduit() {
		return this.idProduit;
	}

	public void setIdProduit(int idProduit) {
		this.idProduit = idProduit;
	}

	public short getPrixUnitaire() {
		return this.prixUnitaire;
	}

	public void setPrixUnitaire(short prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}

	public short getQuantite() {
		return this.quantite;
	}

	public void setQuantite(short quantite) {
		this.quantite = quantite;
	}

}