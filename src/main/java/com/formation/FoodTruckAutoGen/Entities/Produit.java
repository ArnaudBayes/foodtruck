package com.formation.FoodTruckAutoGen.Entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the produits database table.
 * 
 */
@Entity
@Table(name="produits")
@NamedQuery(name="Produit.findAll", query="SELECT p FROM Produit p")
public class Produit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_produit")
	private int idProduit;

	@Lob
	private String description;

	@Column(name="id_famille_repas")
	private int idFamilleRepas;

	@Column(name="libelle_commercial")
	private String libelleCommercial;

	@Column(name="libelle_technique")
	private String libelleTechnique;

	@Column(name="prix_unitaire")
	private byte prixUnitaire;

	private int stock;

	public Produit() {
	}

	public int getIdProduit() {
		return this.idProduit;
	}

	public void setIdProduit(int idProduit) {
		this.idProduit = idProduit;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getIdFamilleRepas() {
		return this.idFamilleRepas;
	}

	public void setIdFamilleRepas(int idFamilleRepas) {
		this.idFamilleRepas = idFamilleRepas;
	}

	public String getLibelleCommercial() {
		return this.libelleCommercial;
	}

	public void setLibelleCommercial(String libelleCommercial) {
		this.libelleCommercial = libelleCommercial;
	}

	public String getLibelleTechnique() {
		return this.libelleTechnique;
	}

	public void setLibelleTechnique(String libelleTechnique) {
		this.libelleTechnique = libelleTechnique;
	}

	public byte getPrixUnitaire() {
		return this.prixUnitaire;
	}

	public void setPrixUnitaire(byte prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}

	public int getStock() {
		return this.stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

}