package com.formation.FoodTruckAutoGen.Entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;


/**
 * The persistent class for the repas database table.
 * 
 */
@Entity
@Table(name="repas")
@NamedQuery(name="Repa.findAll", query="SELECT r FROM Repa r")
public class Repa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_repas")
	private int idRepas;

	private byte activite;

	@Column(name="heure_limite_commande")
	private Time heureLimiteCommande;

	@Column(name="type_repas")
	private String typeRepas;

	public Repa() {
	}

	public int getIdRepas() {
		return this.idRepas;
	}

	public void setIdRepas(int idRepas) {
		this.idRepas = idRepas;
	}

	public byte getActivite() {
		return this.activite;
	}

	public void setActivite(byte activite) {
		this.activite = activite;
	}

	public Time getHeureLimiteCommande() {
		return this.heureLimiteCommande;
	}

	public void setHeureLimiteCommande(Time heureLimiteCommande) {
		this.heureLimiteCommande = heureLimiteCommande;
	}

	public String getTypeRepas() {
		return this.typeRepas;
	}

	public void setTypeRepas(String typeRepas) {
		this.typeRepas = typeRepas;
	}

}