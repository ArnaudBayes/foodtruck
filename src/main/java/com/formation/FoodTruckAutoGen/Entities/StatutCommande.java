package com.formation.FoodTruckAutoGen.Entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the statut_commande database table.
 * 
 */
@Entity
@Table(name="statut_commande")
@NamedQuery(name="StatutCommande.findAll", query="SELECT s FROM StatutCommande s")
public class StatutCommande implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_statut_commande")
	private int idStatutCommande;

	private String libelle;

	public StatutCommande() {
	}

	public int getIdStatutCommande() {
		return this.idStatutCommande;
	}

	public void setIdStatutCommande(int idStatutCommande) {
		this.idStatutCommande = idStatutCommande;
	}

	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

}